# Run this script with administrative privileges
# Uncomment and input the AD groups you want to search for.

# Example Groups
#$groups = ('Domain Admin Users', 'Domain Admins')


$table = @()
Foreach ($group in $groups) {
  Get-ADGroupMember -Identity $group | ForEach-Object {
    $user = Get-ADUser -Identity $PSItem.SamAccountName -Properties *
    $table += New-Object PSObject -Property @{
      Disabled = If($user.Enabled -eq 'false'){'FALSE'} Else {"TRUE"}
      Name = $user.Name
      Distinguished_Name = $user.DistinguishedName
      Group = $group
      Username = $user.UserPrincipalName
      LastLogin = $user.LastLogonDate
      Description = $user.Description
      DisplayName = $user.DisplayName
    }
  }
}
$table | ft -AutoSize
$table | Export-CSV C:\audit-report.csv
