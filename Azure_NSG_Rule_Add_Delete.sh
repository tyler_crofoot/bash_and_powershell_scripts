#!/bin/bash
# This Bash script can be used to add new rules to existing NSGs
# It will read each line of the nsg-list-sorted.txt file and intake some info on each NSG
# This info becomes values for these three variables= $sig, $grp, $nsgname in CLi command
# This while loop will apply the new rule to all NSGs in file = nsg-list-sorted.txt.
#
#
# To run this bash script please execute a command following this format:
# ./azurecli_nsg.sh <MY_NSG_RULE> <PRIORITY_NUM> <ACCESS_TYPE> <RULE_DESCRIPTION> <RULE_DIRECTION> <RULE_PROTOCOL> <SOURCE_ADD_PREFIXES> <SOURCE_PORT_RANGE> <DESTINATION_ADD_PREFIXES> <DESTINATION_PORT_RANGE>
#__________________________________________________________________________________________________________________________________________

# Variables
DESIRED_ACTION=$1
MY_NSG_RULE=$2
FILENAME="nsg-test-list-sorted.txt"


while read -r sid rgp vnet nsgname ipadd;
do
        if [[ $DESIRED_ACTION == "add" ]]; then
                # Variables
                PRIORITY_NUM=$3
                ACCESS_TYPE=$4
                RULE_DESCRIPTION=$5
                RULE_DIRECTION=$6
                RULE_PROTOCOL=$7
                SOURCE_ADD_PREFIXES=$8
                SOURCE_PORT_RANGE=$9
                DESTINATION_ADD_PREFIXES=${10}
                DESTINATION_PORT_RANGE=${11}

                        #This if statement allows for * to be passed in as a value. User must specify "any" to get * passed into variables listed below.
                        if [[ $RULE_PROTOCOL == "any" || $SOURCE_PORT_RANGE == "any" || $DESTINATION_PORT_RANGE == "any" ]]; then
                                RULE_PROTOCOL="*" SOURCE_PORT_RANGE="*" DESTINATION_PORT_RANGE="*"
                        fi
                # This command adds a rule to NSG's. "For $Description specify RFC/Ticket Related to work. EX. AllowPHinboundRFC-8675309"
                az network nsg rule create --subscription $sid --resource-group $rgp --nsg-name $nsgname --name $MY_NSG_RULE --priority $PRIORITY_NUM --access $ACCESS_TYPE --description $RULE_DESCRIPTION  --direction $RULE_DIRECTION --protocol "$RULE_PROTOCOL" --source-address-prefixes $SOURCE_ADD_PREFIXES --source-port-ranges "$SOURCE_PORT_RANGE" --destination-address-prefixes $DESTINATION_ADD_PREFIXES --destination-port-ranges "$DESTINATION_PORT_RANGE"
        
        elif [[ $DESIRED_ACTION ==  "remove" ]]; then
                #  This command will delete rules from NSGs.
                az network nsg rule delete --subscription $sid --resource-group $rgp --nsg-name $nsgname --name $MY_NSG_RULE;
        else
                echo "Please choose to either add or remove a rule from the NSG's listed in nsg-test-list-sorted.txt"
        fi
done < $FILENAME
