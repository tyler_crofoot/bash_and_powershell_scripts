#!/bin/bash
# This script can be used to create & destroy a Azure File share.
#
# If running script from command line and wanting to create or delete multi. file shares place brackets [] around name, choice, and quota variables enetires and comma seperate your inputs. 
#
# Usage: ./file_share_creation.sh <AZURE_STORAGE_ACCOUNT> <SUBSCRIPTION_ID> <FILE_SHARE_NAME> <AZURE_STORAGE_KEY> <FILE_SHARE_CHOICE> <QUOTA>
#---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#Variables
AZURE_STORAGE_ACCOUNT=$1
AZURE_STORAGE_KEY=$2
SUBSCRIPTION_ID=$3
IFS=',' read -ra FILE_SHARE_NAME <<< "$4"
IFS=',' read -ra FILE_SHARE_CHOICE <<< "$5"
IFS=',' read -ra QUOTA <<< "$6"


for (( i=0; i<${#FILE_SHARE_NAME[@]}; i++))
do
    #If you want to create the file share enter create and the steps following create will execute. Otherwise enter delete and it will destroy the file share.
    if [[ ${FILE_SHARE_CHOICE[$i]} == "create" ]] ; then
                az storage share create --account-name $AZURE_STORAGE_ACCOUNT --account-key $AZURE_STORAGE_KEY --name ${FILE_SHARE_NAME[$i]} --quota ${QUOTA[$i]} --subscription $SUBSCRIPTION_ID
    elif [[ ${FILE_SHARE_CHOICE[$i]} == "delete" ]] ; then
                az storage share delete --account-name $AZURE_STORAGE_ACCOUNT --account-key $AZURE_STORAGE_KEY --name ${FILE_SHARE_NAME[$i]} --subscription $SUBSCRIPTION_ID
    else
        	echo "You have not entered a valid request. Please choose to either create or delete a file share."
done
