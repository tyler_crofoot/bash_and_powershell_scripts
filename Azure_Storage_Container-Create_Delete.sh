#/bin/bash
# This script can be used to create and destroy blob storage containers for a specified subscription/resource group.

# Usage: ./storage_container_creation.sh <resource_group> <subscription_ID> <my_storage_container> <azure_storage_key> <azure_storage_account> <storage_container_choice>
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# Variables
resource_group=$1
subscription_ID=$2
my_storage_container=$3
azure_storage_key=$4
azure_storage_account=$5
storage_container_choice=$6

# Gather information of storage account you want to create container in.
for container in $(az storage account list --resource-group $resource_group --subscription $subscription_ID):
do

# If you want to create the container the steps following create will execute. Otherwise it will destroy container.
    if [ $storage_container_choice == "create" ] ; then
        $(az storage container create --name $my_storage_container --account-key $azure_storage_key --account-name $azure_storage_account --fail-on-exist --subscription $subscription_ID)
    elif [ $storage_container_choice == "delete" ] ; then
        $(az storage container delete --name $my_storage_container --account-key $azure_storage_key --account-name $azure_storage_account --fail-on-exist --subscription $subscription_ID)
    else
        echo "You have not entered a valid request. Please choose to either create or delete a storage container."
# Prints out the details of the storage container. 
    $(az storage container show --name $my_storage_container --account-key $azure_storage_key --account-name $azure_storage_account --fail-on-exist --subscription $subscription_ID)

fi
done
